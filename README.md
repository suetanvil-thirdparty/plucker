# Plucker 1.8

Plucker is (was?) an offline HTML (and more) reader for PalmOS
devices.  It consists of two parts: the distiller and the viewer.

The distiller is a computer program that scrapes your preferred
websites and packages them as Plucker documents in PalmOS databases.
These would then be synced to the PalmOS device via its cradle where
the second part, the reader, would let you read the web page's
contents at your leisure.

It is released under the terms of the GNU GPL.

This is a mirror of the last release put here on the off chance that
it would be useful to someone.
